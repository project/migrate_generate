<?php


class BeerTermGenerateMigration extends BeerTermMigration {
  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->description .= ' ' . t('(generated)');
    $this->source = new MigrateGenerateSource(30, $this, array(
      'style' => array(
        'callback' => 'BeerTermGenerateMigration::getBeerStyle',
        'field description' => 'md5() output of the row number',
      ),
      'hoppiness' => array(
         'plugin' => 'MigrateGenerateString',
         'random' => TRUE,
      ),
      'details' => 'MigrateGenerateString',
      'style_parent' => array(
        'plugin' => 'MigrateGenerateSelfReference',
        'random' => TRUE,
      ),
      // Get some greek for our region
      'region' => 'MigrateGenerateDevelGreeking',
    ));
  }

  public function getBeerStyle($options, $field_name, $migration) {
    // It's ugly and longer than is realistic but our data structure supports it
    return 'beer style ' . md5($migration->getSource()->currentRowIDx());
  }
}

class BeerUserGenerateMigration extends BeerUserMigration {
  public function __construct($arguments) {
    $this->description .= ' ' . t('(generated)');
    parent::__construct($arguments);
    $this->source = new MigrateGenerateSource(40, $this, array(
      'aid' => 'MigrateGenerateSerial',
      'status' => 'MigrateGenerateBool',
      'posted' => 'MigrateGenerateDate',
      'name' => 'MigrateGenerateString',
      'password' => 'MigrateGenerateString',
      'mail' => 'MigrateGenerateEmail',
      'sex' => 'MigrateGenerateBool',
      'beers' => 'MigrateGenerateEmpty',
      'nickname' => 'MigrateGenerateEmpty',
    ));
  }
}
class BeerNodeGenerateMigration extends BeerNodeMigration {
  public function __construct($arguments) {
    $this->description .= ' ' . t('(generated)');
    parent::__construct($arguments);
    $this->removeFieldMapping('field_migrate_example_image:source_dir');
    $this->addUnmigratedDestinations(array('field_migrate_example_image:source_dir'));
    $this->source = new MigrateGenerateSource(30, $this, array(
      'bid' => 'MigrateGenerateSerial',
      'name' => 'MigrateGenerateDevelGreeking',
      'body' => 'MigrateGenerateDevelGreeking',
      'excerpt' => 'MigrateGenerateDevelGreeking',
      'aid' => array(
        'plugin' => 'MigrateGenerateReference',
        'source migration' => 'BeerUser',
      ),
      'countries' => array(
        'plugin' => 'MigrateGenerateMultivalue',
        'multivalue plugin' => 'MigrateGenerateString',
        'multivalue range' => array(1,2),
        'multivalue separator' => '|',
      ),
      'image' => 'MigrateGenerateImage',
      'image_alt' => 'MigrateGenerateDevelGreeking',
      'image_title' => 'MigrateGenerateDevelGreeking',
      'image_description' => 'MigrateGenerateDevelGreeking',
      'terms' => array(
        'plugin' => 'MigrateGenerateMultivalue',
        'multivalue plugin' => 'MigrateGenerateString',
        'multivalue separator' => ',',
        'multivalue range' => array(0, 5),
      ),
    ));
  }
}
class BeerCommentGenerateMigration extends BeerCommentMigration {
  public function __construct($arguments) {
    $this->description .= ' ' . t('(generated)');
    parent::__construct($arguments);
    $this->source = new MigrateGenerateSource(50, $this, array(
      'cid' => 'MigrateGenerateSerial',
      'cid_parent' => 'MigrateGenerateEmpty',
      'cid_parent' => array(
        'plugin' => 'MigrateGenerateSelfReference',
        'random' => TRUE,
      ),
      'name' => 'MigrateGenerateString',
      'mail' => 'MigrateGenerateEmail',
      'aid' => array(
        'plugin' => 'MigrateGenerateReference',
        'source migration' => 'BeerUser',
      ),
      'body' => 'MigrateGenerateDevelGreeking',
      'bid' => array(
        'plugin' => 'MigrateGenerateReference',
        'source migration' => 'BeerNode',
      ),
      'subject' => 'MigrateGenerateString',
    ));
  }
}
