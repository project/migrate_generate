<?php

/**
 * @file
 */

/**
 * Implements hook_migrate_api_alter().
 */
function migrate_generate_example_migrate_api_alter(array &$info) {
  foreach ($info['migrate_example']['migrations'] AS $k => $migration) {
    $new_class_name = str_replace('Migration', '', $migration['class_name']) .'GenerateMigration';
    $info['migrate_example']['migrations'][$k]['class_name'] = $new_class_name;
  }
}
