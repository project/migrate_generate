<?php

/**
 * @file
 * Plugin for generating multivalue field output.
 *
 * Relies on another plugin (specified in the options) for generating the
 * individual values.
 */

class MigrateGenerateMultivalue extends MigrateGeneratePlugin {
  public function getDefaultOptions() {
    return array(
      // Default 1 or none
      'multivalue range' => array(0, 1),
      'multivalue separator' => '|',
    );
  }
  public function getFieldOutput($options, $field_name, $migration) {
    if ($num = mt_rand($options['multivalue range'][0], $options['multivalue range'][1])) {
      $separator = $options['multivalue separator'];
      
      if (isset($options['multivalue plugin'])) {
        $plugin = $options['multivalue plugin'];
        $options['multivalue callback'] = "{$plugin}::getFieldOutput";
      }

      $callback = $options['multivalue callback'];

      foreach (array_keys($options) AS $option) {
        if (preg_match('/^multivalue/', $option)) {
          unset($options[$option]);
        }
      }

      $return = array();
      for ($i = 1; $i <= $num; $i++) {
        $return[] = call_user_func_array($callback, array($options, $field_name, $migration));
      }
      return implode($separator, $return);
    }
    return '';
  }

  public function getFieldDescription($field_name, &$options) {
    if (!empty($options['plugin'])) {
      $desc = $options['plugin']::getDescription($field_name, $options);
    }
    elseif (!empty($options['field description'])) {
      $desc = $options['field description'];
    }
    else {
      $desc = MigrateGeneratePlugin::getFieldDescription($field_name, $options);
    }
    return t('!plugin_desc (with @start to @end values)', array(
      '!plugin_desc' => $desc,
      '@start' => $options['multivalue range'][0],
      '@end' => $options['multivalue range'][1],
    ));
  }
}
