<?php

/**
 * TBD
 * This is a planned plugin for paragraph generation. Only the loripsum plugin
 * is supported at this time.
 */

class MigrateGenerateParagraph extends MigrateGeneratePlugin {
  public function getFieldOutput($options, $field_name, $migration) {
    return migrateGenerateParagraph::getFieldDescription($field_name, $options);
  }

  public function getFieldDescription($field_name, &$options) {
    return t('Placeholder: The MigrateGenerateParagraph plugin has not been fully implemented yet.');
  }
}
