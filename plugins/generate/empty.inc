<?php

/**
 * @file
 * Plugin for generating an empty field (if that even makes sense)
 * 
 * Used when you need to always provide an empty field in your generate
 * migrations.
 * 
 * Usage:
 * $options['plugin'] = 'MigrateGenerateEmpty';
 *
 * There are no other options for this plugin.
 */

class MigrateGenerateEmpty extends MigrateGeneratePlugin {
  public function getFieldOutput($options, $field_name, $migration) {
    return '';
  }

  public function getFieldDescription($field_name, $options) {
    return t('No content is generated for this field');
  }
}
