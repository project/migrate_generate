<?php

/**
 * Generate an image.
 */

class MigrateGenerateImage extends MigrateGeneratePlugin {
  public function defaultOptions() {
    return array(
      'extension' => 'png',
      'min_resolution' => '960x960',
      'max_resolution' => '960x960',
    );
  }

  public function getFieldOutput($options, $field_name, $migration) {
    $options += static::defaultOptions($options);
    module_load_include('inc', 'devel_generate', 'image.devel_generate');
    return devel_generate_image($options['extension'], $options['min_resolution'], $options['max_resolution']);
  }

  public function getFieldDescription() {
    return t('Returns an image generated by devel_generate_image');
  }
}
