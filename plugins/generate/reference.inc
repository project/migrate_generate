<?php

/**
 * @file
 * Generate a reference to a row in another migration or to an index in 
 * a database table.
 *
 * Usage:
 * @code
 * $options['plugin'] = 'MigrateGenerateReference';
 * $options['source migration'] = 'MySourceMigration';
 * @endcode
 *
 * Custom usage:
 * This example, would return a random node id
 *
 * @code
 * $options['plugin'] = 'MigrateGenerateReference';
 * $options['table'] = 'node',
 * $options['key'] = 'nid';
 * @endcode
 */

class MigrateGenerateReference extends MigrateGeneratePlugin {
  public function getFieldOutput($options, $field_name, $migration) {
    /**
     * When referencing your own migration, you don't want to try to
     * add a reference to the first item.
     */
    if (isset($options['skip_first']) && $migration->getSource()->currentRowIDx() <= 1) {
      return '';
    }

    if (isset($options['random']) && $options['random'] && mt_rand(0, 1)) {
      return '';
      return MigrateGenerateEmpty;
    }


    // If the table and key are not provided, determine them from the soruce migration
    if (!isset($options['table']) || !isset($options['key'])) {
      $source_migration = MigrationBase::getInstance($options['source migration']);
      $options['table'] = $source_migration->getMap()->getMapTable();
      $options['key'] = 'sourceid1';
    }

    return db_select($options['table'], 't')
      ->fields('t', array($options['key']))
      ->orderRandom()
      ->execute()
      ->fetchField();
  }
  public function getFieldDescription($field_name, &$options) {
    return t('Randomly selected reference to a row in the @source_migration migration', array(
      '@source_migration' => $options['source migration'],
    ));
  }
}

class MigrateGenerateSelfReference extends MigrateGenerateReference {
  public function getFieldOutput($options, $field_name, $migration) {
    if ($migration->getSource()->currentRowIDx() > 1) {
      $options += array(
        'source migration' => $migration->getMachineName(),
        'skip first' => TRUE,
      );
      return MigrateGenerateReference::getFieldOutput($options, $field_name, $migration);
    }
  }

  public function getFieldDescription($field_name, &$options) {
    return t('Randomly generated self-reference');
  }
}
