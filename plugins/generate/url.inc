<?php

/**
 * @file
 * Plugin for generating URLs
 */

class MigrateGenerateURL extends MigrateGeneratePlugin {
  public function getFieldOutput($options, $field_name, $migration) {
    return 'http://somewhere.com';
  }

  public function getFieldDescription($field_name, &$options) {
    // This should be more descriptive
    return t('A randomly generated URL');
  }
}
