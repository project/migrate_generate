<?php

/**
 * Generate phone number.
 */

class MigrateGeneratePhone extends MigrateGeneratePlugin {
  public function defaultOptions() {
    return array(
      'countries' => array('us'),
      'us' => array(
        '800-321-0123',
        '808-474-1234',
      ),
      'ca' => array(
        '+ 1 819 555 5555',
      ),
    );

  }
  public function getFieldOutput($options, $field_name, $migration) {
    $options += static::defaultOptions();
    $country_idr = array_rand($options['countries']);
    $country = $options['countries'][$country_idr];
    return $options[$country];
  }
}
