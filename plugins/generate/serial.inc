<?php

/**
 * @file
 * Plugin for generating serials (primary keys) for migrations.
 * 
 * This plugin presently does not support any options.
 * 
 * Usage:
 * $options['plugin'] = 'MigrateGenerateSerial';
 */

class MigrateGenerateSerial extends MigrateGeneratePlugin {
  public function getFieldOutput($options, $field_name, $migration) {
    $serials = &drupal_static('MigrateGenerateSerial');

    $migration_name = $migration->getMachineName();

    // Initialize our serial storage
    if (!isset($serials[$migration_name])) {
      $serials[$migration_name] = array();
    }
    if (!isset($serials[$migration_name][$field_name])) {
      $serials[$migration_name][$field_name] = 0;
    }

    return ++$serials[$migration_name][$field_name];
  }

  public function getFieldDescription($field_name, $options) {
    return t('Generated serial by MigrateGenerateSerial');
  }
}
