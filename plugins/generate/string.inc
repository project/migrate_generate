<?php

class MigrateGenerateString extends MigrateGeneratePlugin {

  public function getFieldOutput($options, $field_name, $migration) {
    if (isset($options['random']) && $options['random'] && !mt_rand(0,1)) {
      return '';
    }
    $length = isset($options['length']) ? $options['length'] : mt_rand(2,18);
    module_load_include('inc', 'devel_generate');
    return deveL_generate_word($length);
  }
}
