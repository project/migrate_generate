<?php

/**
 * @file
 * Plugin for generating words.
 */

class MigrateGenerateDevelGreeking extends MigrateGeneratePlugin {
  public function getFieldOutput($options, $field_name, $migration) {
    if (isset($options['range'])) {
      $options['word count'] = mt_rand($options['range'][0], $options['range'][1]);
    }
    $options += array(
      'word count' => 1,
      'title' => FALSE
    );
    module_load_include('inc', 'devel_generate');
    return str_replace('.', '', devel_create_greeking($options['word count'], $options['title']));
  }
}
