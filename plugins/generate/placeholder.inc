<?php


class MigrateGeneratePlaceholder extends MigrateGeneratePlugin {

  public function getFieldOutput($options, $field_name, $migration) {
    return $options['placeholder'];
  }

  public function getFieldDescription($field_name, &$options) {
    return t('Will always be populated with "@placeholder"', array(
      '@placeholder' => $options['placeholder'],
    ));
  }
}
