<?php

/**
 * @file
 * Plugin for generating integers.
 *
 * Usage:
 * Specify a range to generate the integer between. This example shows a
 * range from 0 to 10.
 * @code
 * $options['min'] = 0;
 * $options['max'] = 10;
 * @endcode
 */

class MigrateGenerateInt extends MigrateGeneratePlugin {
  public function defaultOptions() {
    return array(
      'min' => 0,
      'max' => 1000
    );
  }
  public function getFieldOutput($options, $field_name, $migration) {
    $options += static::defaultOptions();
    return mt_rand($options['min'], $options['max']);
  }
  public function getFieldDescription($field_name, &$options) {
    return t('A randomly generated integer between a minimum and maximum value.');
  }
}
