<?php

/**
 * @file
 * Generate an email
 */

class MigrateGenerateEmail extends MigrateGeneratePlugin {
  public function getFieldOutput($options, $field_name, $migration) {
    return 'somebody@somewhere.com';
  }

  public function getFieldDescription($field_name, &$options) {
    return t('A poorly generated (static) email address');
  }
}
