<?php

/**
 * @file
 * Plugin for generating dates.
 *
 * Usage:
 * @code
 * $options['plugin'] = 'MigrateGenerateDate';
 * $options['format'] = 'm-d-Y';
 * @endcode
 *
 * Specify a date range to generate dates between. This example shows a
 * range from 3 years ago until now.
 * @code
 * $options['range'] = array(mktime(0, 0, 0, date('m'), date('d'), date('Y')-3, time());
 * @endcode
 */

class MigrateGenerateDate extends MigrateGeneratePlugin {
  public function defaultOptions() {
    return array(
      'format' => 'Y-m-d',
    );
  }
  public function getFieldOutput($options, $field_name, $migration) {
    $options += static::defaultOptions();
    // Provide a range of unix timestamps for the date to be selected from
    if (isset($options['range'])) {
      $options['time'] = mt_rand($options['range'][0], $options['range'][1]);
    }

    // No time provided, generate random time
    if (!isset($options['time'])) {
      $options['time'] = mt_rand(0, time());
    }

    return date($options['format'], $options['time']);
  }

  public function getFieldDescription($field_name, &$options) {
    // This could be more descriptive
    return t('A randomly generated date');
  }
}
